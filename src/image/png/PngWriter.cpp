//
// Created by jesse on 01-08-19.
//

#include <fstream>
#include <functional>
#include <memory>

#include <png.h>
#include <iostream>

#include "PngWriter.hpp"

bool share::PngWriter::write(share::Image& image, const std::string& filename)
{
	std::ofstream file(filename, std::ios::out | std::ios::binary);
	if (!file.is_open()) {
		throw std::runtime_error("could not open file for writing");
	}

	std::unique_ptr<png_struct, std::function<void(png_struct*)>> png(
			png_create_write_struct(PNG_LIBPNG_VER_STRING,
			                        nullptr,
			                        [](png_struct* png, const char* msg) {
				                        throw std::runtime_error(msg);
			                        },
			                        [](png_struct* png, const char* msg) {
				                        throw std::runtime_error(msg);
			                        }),
			[](png_struct* png) {
				png_destroy_write_struct(&png, nullptr);
			});

	if (!png) {
		throw std::runtime_error("could not initialize png struct");
	}

	std::unique_ptr<png_info, std::function<void(png_info*)>> info(
			png_create_info_struct(png.get()),
			[&png](png_info* info) {
				png_destroy_info_struct(png.get(), &info);
			});

	if (!info) {
		throw std::runtime_error("could not initialize png info struct");
	}

	png_set_write_fn(png.get(),
	                 reinterpret_cast<png_voidp>(&file),
	                 [](png_struct* png, png_byte* data, size_t length) {
		                 auto* file = reinterpret_cast<std::ofstream*>(png_get_io_ptr(png));
		                 file->write(reinterpret_cast<const char*>(data), length);
	                 },
	                 [](png_struct* png) {
		                 auto* file = reinterpret_cast<std::ofstream*>(png_get_io_ptr(png));
		                 file->flush();
	                 });

	png_set_IHDR(png.get(), info.get(),
	             image.getWidth(),
	             image.getHeight(),
	             8,
	             image.getDepth() == 32 ? PNG_COLOR_TYPE_RGBA : PNG_COLOR_TYPE_RGB,
	             PNG_INTERLACE_NONE,
	             PNG_COMPRESSION_TYPE_DEFAULT,
	             PNG_FILTER_TYPE_DEFAULT);

	std::vector<uint8_t*> rowPointers(image.getHeight());

	for (size_t i = 0; i < image.getHeight(); ++i) {
		rowPointers[i] = image.getDibPointer() + image.getRowStride() * i;
	}

	png_set_rows(png.get(), info.get(), rowPointers.data());
	png_write_png(png.get(), info.get(), PNG_TRANSFORM_IDENTITY, nullptr);

	return true;
}
