//
// Created by jesse on 01-08-19.
//

#pragma once

#include <string>

#include "image/Image.hpp"

namespace share
{
	class PngWriter
	{
	public:
		static bool write(Image& image, const std::string& filename);
	};
}
