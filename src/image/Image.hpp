//
// Created by jesse on 01-08-19.
//

#pragma once

#include <cstdint>
#include <vector>
#include <string>

namespace share
{
	/**
	 * Image is a general use abstraction around a pixel buffer in memory, as well as providing capability to load and
	 * save images in a various amount of formats.
	 *
	 * The Image class is not meant to be used directly, though nothing stops you from doing so. The reason for that is
	 * that the Image class makes a few assumptions about how it is going to be used for performance reasons, the user
	 * must exert caution to use the Image class directly.
	 *
	 * The Image class has the following constraints:
	 *  - Only 24-bit (RGB) or 32-bit (RGBA) buffers are valid.
	 *  - The buffer has to be in the format of RGB triplets or RGBA quads.
	 *  - The bit depth of an individual color must be 8-bit.
	 */
	class Image
	{
	protected:
		std::vector<uint8_t> dib;
		uint32_t width;
		uint32_t height;
		uint8_t depth;

	public:
		/**
		 * Generates an empty image buffer fulfilling the caller's constraints.
		 * @param width
		 * @param height
		 * @param hasTransparency
		 */
		Image(uint32_t width, uint32_t height, bool hasTransparency)
				: width(width), height(height), depth(hasTransparency ? 32 : 24)
		{
			dib.resize(width * height * depth / 8);
		}

		/**
		 * Attempts to load an image from the filesystem. The loader used is guessed by looking at the extension.
		 * @param filename
		 */
		explicit Image(const std::string& filename);

		/**
		 * Generates an image by copying the buffer into the image memory.
		 * @tparam TIterator
		 * @param width
		 * @param height
		 * @param hasTransparency
		 * @param begin
		 * @param end
		 */
		template<typename TIterator>
		Image(uint32_t width, uint32_t height, bool hasTransparency, TIterator begin, TIterator end)
				: dib(begin, end), width(width), height(height), depth(hasTransparency ? 32 : 24)
		{
			// do nothing
		}

		/**
		 * Generates an image from a buffer by moving the image data into its memory.
		 * @param width
		 * @param height
		 * @param hasTransparency
		 * @param data
		 */
		Image(uint32_t width, uint32_t height, bool hasTransparency, std::vector<uint8_t>&& data)
				: dib(std::move(data)), width(width), height(height), depth(hasTransparency ? 32 : 24)
		{
			// do nothing
		}

		void write(const std::string& filename);

		[[nodiscard]]
		uint32_t getWidth() const
		{
			return width;
		}

		[[nodiscard]]
		uint32_t getHeight() const
		{
			return height;
		}

		[[nodiscard]]
		uint8_t getDepth() const
		{
			return depth;
		}

		[[nodiscard]]
		uint32_t getRowStride() const noexcept
		{
			return width * depth / 8;
		}

		[[nodiscard]]
		uint32_t size() const noexcept
		{
			return dib.size();
		}

		uint8_t* getDibPointer() noexcept
		{
			return dib.data();
		}
	};
}