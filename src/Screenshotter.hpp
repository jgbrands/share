//
// Created by jesse on 29-07-19.
//

#pragma once

#include <cstdint>

#include "Screenshot.hpp"

namespace share
{
	class Screenshotter
	{
	public:
		virtual ~Screenshotter() = default;

		virtual Screenshot captureScreen() = 0;

		virtual Screenshot captureRegion(int16_t x, int16_t y, uint16_t width, uint16_t height) = 0;

		virtual Screenshot captureForegroundWindow() = 0;
	};
}
