//
// Created by jesse on 28-07-19.
//

#pragma once

#include "image/Image.hpp"

namespace share
{
	class Screenshot: public Image
	{
	public:
		explicit Screenshot(Image&& image)
				: Image(image)
		{
			// do nothing
		}
	};
}
