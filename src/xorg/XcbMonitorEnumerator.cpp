//
// Created by jesse on 28-07-19.
//

#include <xcb/randr.h>
#include <stdexcept>

#include "XcbMonitorEnumerator.hpp"
#include "XcbOutputInfo.hpp"

share::XcbMonitorEnumerator::XcbMonitorEnumerator(share::XcbConnection& connection)
		: connection(connection)
{
	// do nothing
}

std::vector<share::Monitor> share::XcbMonitorEnumerator::enumerateMonitors()
{
	xcb_generic_error_t* error = nullptr;
	auto screen = connection.getScreen();

	std::unique_ptr<xcb_randr_get_screen_resources_current_reply_t> reply(
			xcb_randr_get_screen_resources_current_reply(
					connection.getHandle(),
					xcb_randr_get_screen_resources_current(connection.getHandle(), screen->root),
					&error));

	if (error) {
		throw std::runtime_error("failed to get screen resources");
	}

	int length = xcb_randr_get_screen_resources_current_outputs_length(reply.get());
	auto* outputs = xcb_randr_get_screen_resources_current_outputs(reply.get());
	xcb_timestamp_t timestamp = reply->config_timestamp;

	std::vector<Monitor> monitors;

	for (int i = 0; i < length; ++i) {
		XcbOutputInfo outputInfo(connection, outputs[i], timestamp);
		if (outputInfo.valid()) {
			monitors.emplace_back(outputInfo.monitor());
		}
	}

	return monitors;
}
