//
// Created by jesse on 27-07-19.
//

#include "XcbConnection.hpp"

share::XcbConnection::XcbConnection()
{
	handle = xcb_connect(nullptr, &screen);
}

share::XcbConnection::~XcbConnection()
{
	xcb_disconnect(handle);
}

const xcb_setup_t* share::XcbConnection::getSetup()
{
	return xcb_get_setup(handle);
}

xcb_screen_t* share::XcbConnection::getScreen()
{
	return getScreen(screen);
}

xcb_screen_t* share::XcbConnection::getScreen(int screenNum)
{
	xcb_screen_iterator_t iterator = xcb_setup_roots_iterator(getSetup());

	for (int i = 0; i < screenNum; ++i) {
		xcb_screen_next(&iterator);
	}

	return iterator.data;
}
