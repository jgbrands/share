//
// Created by jesse on 28-07-19.
//

#pragma once

#include <vector>

#include "platform/Monitor.hpp"
#include "XcbConnection.hpp"

namespace share
{
	class XcbMonitorEnumerator
	{
	protected:
		XcbConnection& connection;

	public:
		explicit XcbMonitorEnumerator(XcbConnection& connection);

		std::vector<Monitor> enumerateMonitors();
	};
}
