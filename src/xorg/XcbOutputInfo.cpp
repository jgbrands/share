//
// Created by jesse on 28-07-19.
//

#include <stdexcept>

#include "XcbOutputInfo.hpp"

share::XcbOutputInfo::XcbOutputInfo(share::XcbConnection& connection, xcb_randr_output_t handle,
                                    xcb_timestamp_t timestamp)
		: connection(connection), handle(handle)
{
	xcb_generic_error_t* error = nullptr;

	std::unique_ptr<xcb_randr_get_output_info_reply_t> output(xcb_randr_get_output_info_reply(
			connection.getHandle(),
			xcb_randr_get_output_info(
					connection.getHandle(),
					handle,
					timestamp
			),
			&error));

	if (error) {
		throw std::runtime_error("could not get output info");
	}

	if (output->crtc == XCB_NONE || output->connection == XCB_RANDR_CONNECTION_DISCONNECTED) {
		return;
	}

	crtc.reset(xcb_randr_get_crtc_info_reply(
			connection.getHandle(),
			xcb_randr_get_crtc_info(
					connection.getHandle(),
					output->crtc,
					timestamp
			),
			&error));

	if (error) {
		throw std::runtime_error("could not get CRTC info");
	}
}

share::Monitor share::XcbOutputInfo::monitor()
{
	return share::Monitor(crtc->x, crtc->y, crtc->width, crtc->height);
}
