//
// Created by jesse on 27-07-19.
//

#include <stdexcept>
#include <memory>
#include <functional>
#include <iostream>

#include <xcb/shm.h>
#include <xcb/xcb_image.h>

#include "image/Image.hpp"
#include "XcbScreenshotter.hpp"
#include "XcbMonitorEnumerator.hpp"

share::XcbScreenshotter::XcbScreenshotter(share::XcbConnection& connection)
		: connection(connection)
{
	std::unique_ptr<xcb_shm_query_version_reply_t> reply(xcb_shm_query_version_reply(
			connection.getHandle(),
			xcb_shm_query_version(connection.getHandle()),
			nullptr));

	useSharedMemory = reply->shared_pixmaps &&
	                  reply->pixmap_format == XCB_IMAGE_FORMAT_Z_PIXMAP;
}

share::Screenshot share::XcbScreenshotter::captureScreen()
{
	auto* screen = connection.getScreen();
	uint16_t width = screen->width_in_pixels;
	uint16_t height = screen->height_in_pixels;

	return captureRegion(0, 0, width, height);
}

share::Screenshot share::XcbScreenshotter::captureRegion(int16_t x, int16_t y, uint16_t width, uint16_t height)
{
	auto* screen = connection.getScreen();
	uint16_t screenWidth = screen->width_in_pixels;
	uint16_t screenHeight = screen->height_in_pixels;
	xcb_window_t root = screen->root;

	// Sanity checking first, we cannot grab pixels outside the X11 screen.
	x = (x > screenWidth) ? screenWidth - 1 : x;
	y = (y > screenHeight) ? screenHeight - 1 : y;
	x = (x < 0) ? 0 : x;
	y = (y < 0) ? 0 : y;

	width = ((x + width > screenWidth) ? screenWidth - x : width);
	height = ((y + height > screenHeight) ? screenHeight - y : height);

	auto image = captureImage(root, x, y, width, height);
	auto data = convertImage(image.get());

	// We must discard invisible data
	discardInvisiblePixels(data,
	                       XcbMonitorEnumerator(connection).enumerateMonitors(),
	                       x, y);

	return Screenshot(std::move(data));
}

share::Screenshot share::XcbScreenshotter::captureForegroundWindow()
{
	xcb_generic_error_t* error = nullptr;

	std::unique_ptr<xcb_get_input_focus_reply_t> reply(xcb_get_input_focus_reply(
			connection.getHandle(),
			xcb_get_input_focus(connection.getHandle()),
			&error));

	if (error) {
		throw std::runtime_error("could not get input focus window");
	}

	// We do not get the correct window by default, so we must travel down the tree to get top-level window.
	xcb_window_t window = reply->focus;
	while (true) {
		std::unique_ptr<xcb_query_tree_reply_t> tree(xcb_query_tree_reply(
				connection.getHandle(),
				xcb_query_tree(connection.getHandle(), window),
				&error));

		if (error) {
			throw std::runtime_error("error getting query tree");
		}

		if (window == tree->root || tree->parent == tree->root) {
			break;
		}

		window = tree->parent;
	}

	// Now we must retrieve the geometry of this window.
	std::unique_ptr<xcb_get_geometry_reply_t> geometry(xcb_get_geometry_reply(
			connection.getHandle(),
			xcb_get_geometry(connection.getHandle(), window),
			&error));

	if (error) {
		throw std::runtime_error("could not get window geometry");
	}

	auto image = captureImage(window, 0, 0, geometry->width, geometry->height);
	auto data = convertImage(image.get());
	return Screenshot(std::move(data));
}

std::unique_ptr<xcb_image_t, std::function<void(xcb_image_t*)>>
share::XcbScreenshotter::getImageFallback(xcb_drawable_t source, int16_t x, int16_t y, uint16_t w, uint16_t h)
{
	std::unique_ptr<xcb_image_t, std::function<void(xcb_image_t*)>> image(
			xcb_image_get(
					connection.getHandle(),
					source, x, y, w, h,
					0x00FFFFFF,
					XCB_IMAGE_FORMAT_Z_PIXMAP),
			xcb_image_destroy);

	if (!image) {
		throw std::runtime_error("cannot get image from drawable");
	}

	return image;
}

std::unique_ptr<xcb_image_t, std::function<void(xcb_image_t*)>>
share::XcbScreenshotter::captureImageShm(xcb_drawable_t source, int16_t x, int16_t y, uint16_t w, uint16_t h)
{
	// TODO Implement xcb-shm screen capturing.
	std::cerr << "warning: xcb-shm support is not implemented, falling back to xcb_image_get" << std::endl;
	return getImageFallback(source, x, y, w, h);
}

std::unique_ptr<xcb_image_t, std::function<void(xcb_image_t*)>>
share::XcbScreenshotter::captureImage(xcb_drawable_t source, int16_t x, int16_t y, uint16_t w, uint16_t h)
{
	if (useSharedMemory) {
		return captureImageShm(source, x, y, w, h);
	}

	return getImageFallback(source, x, y, w, h);
}

share::Image share::XcbScreenshotter::convertImage(xcb_image_t* image)
{
	share::Image outImage(image->width, image->height, true);
	auto* dib = outImage.getDibPointer();

	// Let's get down to business, what kind of format are we dealing with here?
	if (image->format == XCB_IMAGE_FORMAT_XY_PIXMAP) {
		throw std::runtime_error("xy pixmaps are not supported, use z pixmaps instead");
	} else if (image->format == XCB_IMAGE_FORMAT_Z_PIXMAP) {
		for (uint16_t y = 0; y < image->height; ++y) {
			for (uint16_t x = 0; x < image->width; ++x) {
				auto offset = (y * image->width + x) * 4;

				// X11 ZPixmaps are in BGRA format, so we must flip the bits.
				dib[offset + 0] = image->data[offset + 2];
				dib[offset + 1] = image->data[offset + 1];
				dib[offset + 2] = image->data[offset + 0];
				dib[offset + 3] = 255;
			}
		}
	} else {
		throw std::runtime_error("unsupported xcb_image_format");
	}

	return outImage;
}

void share::XcbScreenshotter::discardInvisiblePixels(share::Image& image, const std::vector<Monitor>& monitors,
                                                     int16_t ox, int16_t oy)
{
	// TODO: A more efficient way would be to create a vector of rectangles that have the garbage data.
	//       Should consider doing that soon-ish, maybe? For now, this works but is slow.

	auto* dib = image.getDibPointer();

	for (uint32_t y = 0; y < image.getHeight(); ++y) {
		for (uint32_t x = 0; x < image.getWidth(); ++x) {
			// Is this pixel part of the visible display?
			bool partOfVisibleDisplay = false;

			auto vx = ox + x;
			auto vy = oy + y;

			for (const auto& monitor: monitors) {
				if (vx >= static_cast<uint32_t>(monitor.getXOrigin())
				    && vx <= monitor.getXOrigin() + monitor.getWidth()
				    && vy >= static_cast<uint32_t>(monitor.getYOrigin())
				    && vy <= monitor.getYOrigin() + monitor.getHeight()) {
					partOfVisibleDisplay = true;
					break;
				}
			}

			if (!partOfVisibleDisplay) {
				auto offset = (y * image.getWidth() + x) * image.getDepth() / 8;

				dib[offset + 0] = 0;
				dib[offset + 1] = 0;
				dib[offset + 2] = 0;

				if (image.getDepth() == 32) {
					dib[offset + 3] = 0;
				}
			}
		}
	}
}
