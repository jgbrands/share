//
// Created by jesse on 29-07-19.
//

#pragma once

#include <memory>

#include "Runtime.hpp"
#include "XcbConnection.hpp"

namespace share
{
	class XcbRuntime: public Runtime
	{
	protected:
		XcbConnection connection;

	public:
		std::unique_ptr<Screenshotter> createScreenshotter() override;

		[[nodiscard]]
		XcbConnection& getConnection()
		{
			return connection;
		}
	};
}
