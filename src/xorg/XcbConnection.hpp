//
// Created by jesse on 27-07-19.
//

#pragma once

#include <xcb/xcb.h>

namespace share
{
	class XcbConnection
	{
	protected:
		xcb_connection_t* handle;
		int screen;

	public:
		XcbConnection();

		XcbConnection(const XcbConnection&) = delete;

		XcbConnection(XcbConnection&& other) noexcept
				: handle(other.handle), screen(other.screen)
		{
			other.handle = nullptr;
		}

		~XcbConnection();

		XcbConnection& operator=(XcbConnection&) = delete;

		XcbConnection& operator=(XcbConnection&& other) noexcept
		{
			xcb_disconnect(handle);
			handle = other.handle;
			screen = other.screen;
			other.handle = nullptr;

			return *this;
		}

		const xcb_setup_t* getSetup();

		xcb_screen_t* getScreen();

		xcb_screen_t* getScreen(int screenNum);

		[[nodiscard]]
		xcb_connection_t* getHandle() const
		{
			return handle;
		}
	};
}