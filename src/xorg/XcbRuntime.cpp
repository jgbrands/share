//
// Created by jesse on 29-07-19.
//

#include "XcbRuntime.hpp"
#include "XcbScreenshotter.hpp"

std::unique_ptr<share::Screenshotter> share::XcbRuntime::createScreenshotter()
{
	return std::make_unique<XcbScreenshotter>(connection);
}
