//
// Created by jesse on 27-07-19.
//

#pragma once

#include <cstdint>
#include <memory>
#include <functional>

#include <xcb/xcb_image.h>

#include "Screenshot.hpp"
#include "Screenshotter.hpp"
#include "XcbConnection.hpp"
#include "image/Image.hpp"
#include "platform/Monitor.hpp"

namespace share
{
	class XcbScreenshotter : public Screenshotter
	{
		std::unique_ptr<xcb_image_t, std::function<void(xcb_image_t*)>>
		getImageFallback(xcb_drawable_t source, int16_t x, int16_t y, uint16_t w, uint16_t h);

		std::unique_ptr<xcb_image_t, std::function<void(xcb_image_t*)>>
		captureImageShm(xcb_drawable_t source, int16_t x, int16_t y, uint16_t w, uint16_t h);

		std::unique_ptr<xcb_image_t, std::function<void(xcb_image_t*)>>
		captureImage(xcb_drawable_t source, int16_t x, int16_t y, uint16_t w, uint16_t h);

		share::Image convertImage(xcb_image_t* image);

		/**
		 * Discards the invisible pixels that may have been captured when copying over the pixel buffer of the X11
		 * display. The X11 virtual display may have areas that cannot be seen by the user (for example, multi monitor
		 * set ups with different resolutions). For optimization reasons, X11 does not clear out the data on repaints
		 * and this results in a lot of visual garbage. This function zeroes out all data in the invisible areas.
		 * @param image The image to modify.
		 * @param monitors Vector of the monitors attached to the display, obtained by XcbMonitorEnumerator
		 * @param ox The x-offset from the virtual display origin.
		 * @param oy The y-offset from the virtual display origin.
		 */
		void
		discardInvisiblePixels(share::Image& image, const std::vector<Monitor>& monitors, int16_t ox, int16_t oy);

	protected:
		XcbConnection& connection;
		bool useSharedMemory;

	public:
		explicit XcbScreenshotter(XcbConnection& connection);

		Screenshot captureScreen() override;

		Screenshot captureRegion(int16_t x, int16_t y, uint16_t width, uint16_t height) override;

		Screenshot captureForegroundWindow() override;

		[[nodiscard]]
		XcbConnection& getConnection() const
		{
			return connection;
		}
	};
}
