//
// Created by jesse on 28-07-19.
//

#pragma once

#include <xcb/randr.h>
#include <memory>

#include "platform/Monitor.hpp"
#include "XcbConnection.hpp"

namespace share
{
	class XcbOutputInfo
	{
	protected:
		XcbConnection& connection;
		xcb_randr_output_t handle;
		std::unique_ptr<xcb_randr_get_crtc_info_reply_t> crtc;

	public:
		XcbOutputInfo(XcbConnection& connection, xcb_randr_output_t handle, xcb_timestamp_t timestamp);

		Monitor monitor();

		bool valid()
		{
			return crtc != nullptr;
		};
	};
}
