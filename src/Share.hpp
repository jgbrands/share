//
// Created by jesse on 27-07-19.
//

#pragma once

#include <memory>

#include "Runtime.hpp"
#include "Screenshotter.hpp"

namespace share
{
	class Share
	{
	protected:
		std::unique_ptr<Runtime> runtime;

	public:
		Share();

		virtual ~Share() = default;

		[[nodiscard]]
		const std::unique_ptr<Runtime>& getRuntime() const
		{
			return runtime;
		}
	};
}
