//
// Created by jesse on 29-07-19.
//

#include <memory>

#include "Share.hpp"
#include "xorg/XcbRuntime.hpp"

share::Share::Share()
{
#ifdef LIBSHARE_X11_ENABLED
	runtime = std::make_unique<XcbRuntime>();
#endif

	if (!runtime) {
		throw std::runtime_error("could not initialize libshare");
	}
}
