//
// Created by jesse on 28-07-19.
//

#pragma once

#include <cstdint>

namespace share
{
	class Monitor
	{
	protected:
		int32_t xOrigin;
		int32_t yOrigin;
		uint32_t width;
		uint32_t height;

	public:
		Monitor(int32_t x, int32_t y, uint32_t width, uint32_t height)
				: xOrigin(x), yOrigin(y), width(width), height(height)
		{
			// do nothing
		}

		[[nodiscard]]
		int32_t getXOrigin() const
		{
			return xOrigin;
		}

		[[nodiscard]]
		int32_t getYOrigin() const
		{
			return yOrigin;
		}

		[[nodiscard]]
		uint32_t getWidth() const
		{
			return width;
		}

		[[nodiscard]]
		uint32_t getHeight() const
		{
			return height;
		}
	};
}
