//
// Created by jesse on 29-07-19.
//

#pragma once

#include <memory>

#include "Screenshotter.hpp"

namespace share
{
	class Runtime
	{
	public:
		virtual ~Runtime() = default;

		virtual std::unique_ptr<Screenshotter> createScreenshotter() = 0;
	};
}
